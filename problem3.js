function problem3(data) {
  try {
    if (!Array.isArray(data) || data.length === 0) {
      throw new Error("Input data is not a non-empty array.");
    }

    let sortedCarModelNames = [];

    for (let index = 0; index < data.length; index++) {
      if (data[index].car_model) {
        sortedCarModelNames.push(data[index].car_model);
      }
    }
    
    for (
      let currentIndex = 0;
      currentIndex < sortedCarModelNames.length - 1;
      currentIndex++
    ) {
      for (
        let nextIndex = 0;
        nextIndex < sortedCarModelNames.length - 1 - currentIndex;
        nextIndex++
      ) {
        if (
          sortedCarModelNames[nextIndex].localeCompare(
            sortedCarModelNames[nextIndex + 1]
          ) > 0
        ) {
          const temp = sortedCarModelNames[nextIndex];
          sortedCarModelNames[nextIndex] = sortedCarModelNames[nextIndex + 1];
          sortedCarModelNames[nextIndex + 1] = temp;
        }
      }
    }

    return sortedCarModelNames;
  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem3;
