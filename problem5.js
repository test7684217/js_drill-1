function problem5(data){
  try {
    if (!Array.isArray(data) || data.length === 0) {
      throw new Error("Input data is not a non-empty array.");
    }

    let years = [];

    for (let index = 0; index < data.length; index++) {
      if (data[index].car_year && data[index].car_year < 2000) {
        years.push(data[index].car_year);
      }
    }

    return years;
  } catch (error) {
    console.error("An error occurred:", error.message);
    return null;
  }
};


module.exports = problem5