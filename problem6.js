function problem6(data) {
  try {
    if (!Array.isArray(data) || data.length === 0) {
      throw new Error("Input data is not a non-empty array.");
    }

    let result = [];

    for (let index = 0; index < data.length; index++) {
      if (data[index].car_make === "Audi" || data[index].car_make === "BMW") {
        result.push(data[index]);
      }
    }

    return result;
  } catch (error) {
    console.error(error.message);
    return [];
  }
}

module.exports = problem6;
