function problem1(data) {
  try {
    if (!Array.isArray(data)) {
      throw new Error("Input data is not an array.");
    }

    let carFound = false;
    for (let index = 0; index < data.length; index++) {
      if (data[index].id == 33) {
        if (
          !data[index].car_year ||
          !data[index].car_make ||
          !data[index].car_model
        ) {
          throw new Error("Incomplete data for car with ID 33.");
        }
        console.log(
          `Car 33 is a ${data[index].car_year} ${data[index].car_make} ${data[index].car_model}`
        );
        carFound = true;
        break;
      }
    }

    if (!carFound) {
      console.log("Car with ID 33 not found.");
    }
  } catch (error) {
    console.error(error.message);
  }
}

module.exports = problem1;
