function problem2(data) {
  try {
    if (!Array.isArray(data) || data.length === 0) {
      throw new Error("Input data is not a non-empty array.");
    }

    let last_car = data[data.length - 1];
    if (!last_car || !last_car.car_make || !last_car.car_model) {
      throw new Error("Incomplete data for the last car.");
    }

    console.log(`Last car is a ${last_car.car_make} ${last_car.car_model}`);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

module.exports = problem2;
